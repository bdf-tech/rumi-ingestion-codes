# Ingestion codes for Rumi

## fundamental scores ingestion

Companies can be grouped into different industries based on classification system and level (industry, sub-industry, sector, etc).

("gics", "rbics", "naics", "fs", "sic") - are different classification systems.

Each of classification types has different levels - 
For example:

**gics** has 4 levels "Sector", "Industry group", "Sub industry"

**rbics** has 6 levels "Economy", "Sector", "Sub sector", "Industry group", "Industry", "Sub industry"

These levels can be obtained from `Stocks2::industryBreakdown(type = 'rbics')`


output data from the `storeFundamentalScores` function in `fundamental_scores_ingestion.R`

| date | classification_system | classification_level | industry_name | score | 
|-------|---------------------|---------------------|---------------|--------|
|date on which score is created|can be one of ("gics", "rbics", "naics", "fs", "sic") | level to be considered in classification system | industry_name (decided by combination of classification system and classification_level) | score | 